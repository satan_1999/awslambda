# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Steps of Configuring AWS ###

* 1. Create Maven project for the function and add Lambda Function/Jar into AWS
* 2. Add S3 Bucket
* 3. Add SNS 
* 4. Add Event to S3 to post event to SNS
* 5. Add Subscription into SNS for Lambda Function
* 6. Add Subscription into SNS for emailing the S3 event into emailbox

